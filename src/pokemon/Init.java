/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pokemon;

import java.util.Scanner;
import pokemon.utils.Play;
import pokemon.utils.PokemonLists;

/**
 *
 * @author Alejandro Barrios
 */
public class Init {

    Play play;
    PokemonLists pokemonLists;

    public Init() {
        System.out.println("------------------ POKEMON USAC -----------------");
        // Creando pokemons
        Principal.pokemonFunctions.startPokemons();
        pokemonLists = new PokemonLists();
        play = new Play();
        createMenu();
    }

    public void createMenu() {

        int selected = 0;

        System.out.println("---------------------- MENU ----------------------");
        System.out.println("Ingresa el número de la opción correspondiente");
        System.out.println("1. PokeDex");
        System.out.println("2. Editar pokemon");
        System.out.println("3. Registro de partidas");
        System.out.println("4. Batallar");
        System.out.println("5. Listado de pokemones más utilizados");
        System.out.println("6. Listado de pokemones menos utilizados");
        System.out.println("7. Salir");
        System.out.print("> ");

        Scanner selection = new Scanner(System.in);
        try {

            selected = selection.nextInt();
            if (selected > 7 || selected < 1) {
                System.out.println("Ingresa un número entre 1 y 7");
                createMenu();
            }

        } catch (Exception e) {
            System.out.println("Ingresa un número entre 1 y 7");
            createMenu();
        }

        optionSelected(selected);
    }

    public void optionSelected(int option) {

        switch (option) {

            case 1:
                Principal.pokemonFunctions.pokedex();
                break;

            case 2:
                Principal.pokemonFunctions.editPokemon();
                break;

            case 3:
                pokemonLists.historialDePartidas();
                break;

            case 4:

                // Auto increment Array
                play.startGame();
                play.save();
                break;

            case 5:
                pokemonLists.usedPokemons(1); // Trae pokemones mas utilizados
                break;

            case 6:
                pokemonLists.usedPokemons(0); // Trae pokemones menos utilizados
                break;

            case 7:
                System.exit(0);
                break;

        }

        createMenu();

    }

}
