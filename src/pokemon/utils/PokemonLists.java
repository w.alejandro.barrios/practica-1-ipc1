/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pokemon.utils;

import pokemon.Principal;
import pokemon.beans.Pokemon;
import pokemon.beans.Game;

/**
 *
 * @author Alejandro Barrios
 */
public class PokemonLists {

    public void usedPokemons(int selection) {

        Pokemon[] pokemon = Principal.pokemonFunctions.pokemon;
        int[][] datosOrdenados = new int[pokemon.length][2];

        for (int contador = 0; contador < pokemon.length; contador++) {
            datosOrdenados[contador][0] = pokemon[contador].getId();
            datosOrdenados[contador][1] = pokemon[contador].getTimesPlayed();
        }
        
        for(int i = 0; i < datosOrdenados.length-1; i++){
            for(int a = 0; a < datosOrdenados.length-i-1; a++){
                if(datosOrdenados[a][1]<datosOrdenados[a+1][1]){
                    int temporal = datosOrdenados[a+1][1];
                    int idPokemon = datosOrdenados[a+1][0];
                    datosOrdenados[a+1][1] = datosOrdenados[a][1];
                    datosOrdenados[a+1][0] = datosOrdenados[a][0];
                    datosOrdenados[a][1] = temporal;
                    datosOrdenados[a][0] = idPokemon;
                }
            }
        }


        if (selection == 0) {
            System.out.println("POKEMONES MENOS UTILIZADOS");
            for (int contador = 3; contador < 6; contador++) {
                if (datosOrdenados[0][1] == 0) {
                    System.out.println("Aun no has realizado ninguna batalla");
                    contador = 6;
                } else {
                    System.out.println("--------------------------------------------");
                    System.out.println("Pokemon: " + pokemon[datosOrdenados[contador][0]].getName());
                    System.out.println("Seleccionado " + pokemon[datosOrdenados[contador][0]].getTimesPlayed() + " veces");
                    System.out.println(pokemon[datosOrdenados[contador][0]].getImgPokemon());
                    System.out.println("--------------------------------------------");
                }

            }
        } else {
            System.out.println("POKEMONES MÁS UTILIZADOS");
            for (int contador = 0; contador < datosOrdenados.length - 3; contador++) {
                if (datosOrdenados[0][1] == 0) {
                    System.out.println("Aun no has realizado ninguna batalla");
                    contador = datosOrdenados.length;
                } else {
                    System.out.println("--------------------------------------------");
                    System.out.println("Pokemon: " + pokemon[datosOrdenados[contador][0]].getName());
                    System.out.println("Seleccionado " + pokemon[datosOrdenados[contador][0]].getTimesPlayed() + " veces");
                    System.out.println(pokemon[datosOrdenados[contador][0]].getImgPokemon());
                    System.out.println("--------------------------------------------");
                }

            }
        }

    }

    public void historialDePartidas() {
        Game[] game = Principal.game;
        Pokemon[] pokemon = Principal.pokemonFunctions.pokemon;
        System.out.println("--------------- REGISTRO DE PARTIDAS ---------------");
        for (int contador = 0; contador < game.length; contador++) {
            try {
                game[contador].getNamePlayer1();
                System.out.println("Partida No. #" + (contador + 1));
                System.out.println(game[contador].getNamePlayer1() + " vs " + game[contador].getNamePlayer2());
                System.out.println("Pokemones utilizados por " + game[contador].getNamePlayer1());
                System.out.println("1. " + pokemon[game[contador].getPokemonsPlayer1()[0][0]].getName());
                System.out.println("-- Recibió " + game[contador].getPokemonsPlayer1()[0][1]
                        + " ataques y atacó " + game[contador].getPokemonsPlayer1()[0][2] + " veces");
                System.out.println("2. " + pokemon[game[contador].getPokemonsPlayer1()[1][0]].getName());
                System.out.println("-- Recibió " + game[contador].getPokemonsPlayer1()[1][1]
                        + " ataques y atacó " + game[contador].getPokemonsPlayer1()[1][2] + " veces");

                System.out.println("");
                System.out.println("Pokemones utilizados por " + game[contador].getNamePlayer2());
                System.out.println("1. " + pokemon[game[contador].getPokemonsPlayer2()[0][0]].getName());
                System.out.println("-- Recibió " + game[contador].getPokemonsPlayer2()[0][1]
                        + " ataques y atacó " + game[contador].getPokemonsPlayer2()[0][2] + " veces");
                System.out.println("2. " + pokemon[game[contador].getPokemonsPlayer2()[1][0]].getName());
                System.out.println("-- Recibió " + game[contador].getPokemonsPlayer2()[1][1]
                        + " ataques y atacó " + game[contador].getPokemonsPlayer2()[1][2] + " veces");
                System.out.println("");
                System.out.println("Ganador: " + game[contador].getWinner());
                System.out.println("------------------------------------------------");
            }catch(NullPointerException e){
                if(contador == 0){
                   System.out.println("Aún no se ha jugado ninguna partida"); 
                }
                contador = game.length;
            }

        }
    }

}
