/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pokemon.utils;

import java.util.Scanner;
import pokemon.beans.Game;
import pokemon.Principal;
import pokemon.beans.Pokemon;
import java.util.Random;

/**
 *
 * @author Alejandro Barios
 */
public class Play {

    Scanner dataReader = new Scanner(System.in);
    Pokemon[] pokemon;
    Game[] game;
    int gamesCounter;
    int[] lifePlayer1 = new int[2];
    int[] lifePlayer2 = new int[2];
    int[][] pokemonDataP1;
    int[][] pokemonDataP2;
    int turnoJugando;

    public Play() {
        Principal.game = new Game[10];
        Principal.gamesCounter = 0;
        game = Principal.game;
        gamesCounter = Principal.gamesCounter;
        pokemon = Principal.pokemonFunctions.pokemon;
    }

    public void startGame() {

        if (gamesCounter >= game.length) {
            Game[] gameCopy = new Game[game.length];
            int capacity = game.length + 10;
            gameCopy = game;
            game = new Game[capacity];
            for(int contador = 0; contador < gameCopy.length; contador++){
                game[contador] = gameCopy[contador];
            }
        }
        pokemonDataP1 = new int[2][3];
        pokemonDataP2 = new int[2][3];

        game[gamesCounter] = new Game();
        askForData();
        gamesCounter++;

    }

    public void askForData() {

        game[gamesCounter].setId(gamesCounter);

        // JUGADOR #1
        System.out.println("------------------- Jugador 1 -------------------");
        System.out.print("Nombre > ");
        game[gamesCounter].setNamePlayer1(dataReader.next());
        boolean acceptedValue = true;
        int selected = 0;
        do {
            System.out.println("Selecciona tu pokemon #1");
            for (int counter = 0; counter < pokemon.length; counter++) {
                System.out.println((counter + 1) + "." + pokemon[counter].getName());
            }
            System.out.print("> ");

            try {

                //System.out.println(dataReader.close());
                selected = dataReader.nextInt();

                if (selected > pokemon.length || selected < 1) {
                    acceptedValue = false;
                } else {
                    acceptedValue = true;
                }

            } catch (Exception e) {
                e.printStackTrace();
                System.exit(0);
                acceptedValue = false;
            }
        } while (!acceptedValue);

        int firstPokemon = selected - 1;
        pokemonDataP1[0][0] = firstPokemon;
        pokemonDataP1[0][1] = 0;
        pokemonDataP1[0][2] = 0;
        game[gamesCounter].setPokemonsPlayer1(pokemonDataP1);

        do {
            System.out.println("Selecciona tu pokemon #2 (Diferente al #1)");
            for (int counter = 0; counter < pokemon.length; counter++) {
                System.out.println((counter + 1) + "." + pokemon[counter].getName());
            }
            System.out.print("> ");

            try {
                selected = dataReader.nextInt();
                if (selected > pokemon.length || selected < 1 || selected - 1 == game[gamesCounter].getPokemonsPlayer1()[0][0]) {
                    acceptedValue = false;
                } else {
                    acceptedValue = true;
                }

            } catch (Exception e) {
                acceptedValue = false;
            }
        } while (!acceptedValue);

        int secondPokemon = selected - 1;
        pokemonDataP1[1][0] = secondPokemon;
        pokemonDataP1[1][1] = 0;
        pokemonDataP1[1][2] = 0;
        game[gamesCounter].setPokemonsPlayer1(pokemonDataP1);

        // JUGADOR #2
        System.out.println("------------------- Jugador 2 -------------------");
        System.out.print("Nombre > ");
        game[gamesCounter].setNamePlayer2(dataReader.next());
        do {
            System.out.println("Selecciona tu pokemon #1");
            for (int counter = 0; counter < pokemon.length; counter++) {
                System.out.println((counter + 1) + "." + pokemon[counter].getName());
            }
            System.out.print("> ");

            try {
                selected = dataReader.nextInt();
                if (selected > pokemon.length || selected < 1) {
                    acceptedValue = false;
                } else {
                    acceptedValue = true;
                }

            } catch (Exception e) {
                acceptedValue = false;
            }
        } while (!acceptedValue);

        firstPokemon = selected - 1;
        pokemonDataP2[0][0] = firstPokemon;
        pokemonDataP2[0][1] = 0;
        pokemonDataP2[0][2] = 0;
        game[gamesCounter].setPokemonsPlayer2(pokemonDataP2);

        do {
            System.out.println("Selecciona tu pokemon #2 (Diferente al #1)");
            for (int counter = 0; counter < pokemon.length; counter++) {
                System.out.println((counter + 1) + "." + pokemon[counter].getName());
            }
            System.out.print("> ");

            try {
                selected = dataReader.nextInt();
                if (selected > pokemon.length || selected < 1 || selected - 1 == game[gamesCounter].getPokemonsPlayer2()[0][0]) {
                    acceptedValue = false;
                } else {
                    acceptedValue = true;
                }

            } catch (Exception e) {
                acceptedValue = false;
            }
        } while (!acceptedValue);

        secondPokemon = selected - 1;
        pokemonDataP2[1][0] = secondPokemon;
        pokemonDataP2[1][1] = 0;
        pokemonDataP2[1][2] = 0;
        game[gamesCounter].setPokemonsPlayer2(pokemonDataP2);

        printPreviewDetails();

    }

    public void printPreviewDetails() {
        System.out.println("--------------- DETALLES DE PARTIDA ---------------");
        System.out.println("Jugador #1: " + game[gamesCounter].getNamePlayer1());
        for (int contador = 0; contador < pokemon.length; contador++) {
            if (pokemon[contador].getId() == game[gamesCounter].getPokemonsPlayer1()[0][0]) {
                System.out.println("Pokemon #1: " + pokemon[contador].getName());
                pokemon[contador].setTimesPlayed(pokemon[contador].getTimesPlayed() + 1);
            }
        }
        for (int contador = 0; contador < pokemon.length; contador++) {
            if (pokemon[contador].getId() == game[gamesCounter].getPokemonsPlayer1()[1][0]) {
                System.out.println("Pokemon #2: " + pokemon[contador].getName());
                pokemon[contador].setTimesPlayed(pokemon[contador].getTimesPlayed() + 1);
            }
        }
        System.out.println("---------------------------------------------------");
        System.out.println("Jugador #2: " + game[gamesCounter].getNamePlayer2());
        for (int contador = 0; contador < pokemon.length; contador++) {
            if (pokemon[contador].getId() == game[gamesCounter].getPokemonsPlayer2()[0][0]) {
                System.out.println("Pokemon #1: " + pokemon[contador].getName());
                pokemon[contador].setTimesPlayed(pokemon[contador].getTimesPlayed() + 1);
            }
        }
        for (int contador = 0; contador < pokemon.length; contador++) {
            if (pokemon[contador].getId() == game[gamesCounter].getPokemonsPlayer2()[1][0]) {
                System.out.println("Pokemon #2: " + pokemon[contador].getName());
                pokemon[contador].setTimesPlayed(pokemon[contador].getTimesPlayed() + 1);
            }
        }
        System.out.println("---------------------------------------------------");
        fight();
    }

    public void fight() {
        turnoJugando = 0;
        int turno = 0;
        boolean haveWinner = false;
        lifePlayer1[0] = pokemon[game[gamesCounter].getPokemonsPlayer1()[0][0]].getLifePoints();
        lifePlayer1[1] = pokemon[game[gamesCounter].getPokemonsPlayer1()[1][0]].getLifePoints();
        lifePlayer2[0] = pokemon[game[gamesCounter].getPokemonsPlayer2()[0][0]].getLifePoints();
        lifePlayer2[1] = pokemon[game[gamesCounter].getPokemonsPlayer2()[1][0]].getLifePoints();

        Random firstAtaquer = new Random();
        int randomized = firstAtaquer.nextInt(2 - 1) + 1;
        turno = randomized;
        Scanner choose = new Scanner(System.in);
        int selectionToAtk = 0;
        int selectionAtk = 0;
        do {
            if (turno % 2 != 0) {
                mostrarBatalla();
                System.out.println("Turno del jugador: " + game[gamesCounter].getNamePlayer1());
                System.out.println("¿Qué pokemon deseas atacar?");
                System.out.println("1. " + pokemon[game[gamesCounter].getPokemonsPlayer2()[0][0]].getName());
                System.out.println("2. " + pokemon[game[gamesCounter].getPokemonsPlayer2()[1][0]].getName());
                System.out.print("> ");
                selectionToAtk = choose.nextInt();
                System.out.println("¿Con cual pokemon deseas atacar?");
                System.out.println("1. " + pokemon[game[gamesCounter].getPokemonsPlayer1()[0][0]].getName());
                System.out.println("2. " + pokemon[game[gamesCounter].getPokemonsPlayer1()[1][0]].getName());
                System.out.print("> ");
                selectionAtk = choose.nextInt();

                pokemonDataP2[selectionToAtk - 1][1] = game[gamesCounter].getPokemonsPlayer2()[selectionToAtk - 1][1] + 1;
                lifePlayer2[selectionToAtk - 1] -= pokemon[game[gamesCounter].getPokemonsPlayer1()[selectionAtk - 1][0]].getAttakPoints();
                game[gamesCounter].setPokemonsPlayer2(pokemonDataP2);

                pokemonDataP1[selectionAtk - 1][2] = game[gamesCounter].getPokemonsPlayer1()[selectionAtk - 1][2] + 1;
                game[gamesCounter].setPokemonsPlayer1(pokemonDataP1);
                
                if(lifePlayer2[0] <=0 && lifePlayer2[1] <= 0){
                    haveWinner = true;
                    game[gamesCounter].setWinner(game[gamesCounter].getNamePlayer1());
                }
                turno += 1;
            } else {
                mostrarBatalla();
                System.out.println("Turno del jugador: " + game[gamesCounter].getNamePlayer2());
                System.out.println("¿Qué pokemon deseas atacar?");
                System.out.println("1. " + pokemon[game[gamesCounter].getPokemonsPlayer1()[0][0]].getName());
                System.out.println("2. " + pokemon[game[gamesCounter].getPokemonsPlayer1()[1][0]].getName());
                System.out.print("> ");
                selectionToAtk = choose.nextInt();
                System.out.println("¿Con cual pokemon deseas atacar?");
                System.out.println("1. " + pokemon[game[gamesCounter].getPokemonsPlayer2()[0][0]].getName());
                System.out.println("2. " + pokemon[game[gamesCounter].getPokemonsPlayer2()[1][0]].getName());
                System.out.print("> ");
                selectionAtk = choose.nextInt();

                pokemonDataP1[selectionToAtk - 1][1] = game[gamesCounter].getPokemonsPlayer1()[selectionToAtk - 1][1] + 1;
                lifePlayer1[selectionToAtk - 1] -= pokemon[game[gamesCounter].getPokemonsPlayer2()[selectionAtk - 1][0]].getAttakPoints();
                game[gamesCounter].setPokemonsPlayer1(pokemonDataP1);

                pokemonDataP2[selectionAtk - 1][2] = game[gamesCounter].getPokemonsPlayer2()[selectionAtk - 1][2] + 1;
                game[gamesCounter].setPokemonsPlayer2(pokemonDataP2);
                if(lifePlayer1[0] <=0 && lifePlayer1[1] <= 0){
                    haveWinner = true;
                    game[gamesCounter].setWinner(game[gamesCounter].getNamePlayer2());
                }
                turno += 1;
            }
        } while (!haveWinner);
        
        mostrarBatalla();
        System.out.println("BATALLA FINALIZADA!");
        System.out.println("GANADOR: " + game[gamesCounter].getWinner() + "!");

    }

    public void mostrarBatalla() {
        System.out.println("---------------------------------------------------");
        System.out.println("Jugador #1: " + game[gamesCounter].getNamePlayer1());
        System.out.println("Pokemones");
        for (int contador = 0; contador < lifePlayer1.length; contador++) {
            System.out.println(pokemon[game[gamesCounter].getPokemonsPlayer1()[contador][0]].getImgPokemon());
            System.out.println("Nombre: " + pokemon[game[gamesCounter].getPokemonsPlayer1()[contador][0]].getName());
            System.out.println("Vida: " + lifePlayer1[contador]);
            System.out.println("Ataque: " + pokemon[game[gamesCounter].getPokemonsPlayer1()[contador][0]].getAttakPoints());
            if (lifePlayer1[contador] > 0) {
                System.out.println("Estado: Vivo");
            } else {
                System.out.println("Estado: Muerto");
            }
            System.out.println("----------------------");
        }

        System.out.println("---------------------------------------------------");
        System.out.println("Jugador #2: " + game[gamesCounter].getNamePlayer2());
        System.out.println("Pokemones");
        for (int contador = 0; contador < lifePlayer2.length; contador++) {
            System.out.println(pokemon[game[gamesCounter].getPokemonsPlayer2()[contador][0]].getImgPokemon());
            System.out.println("Nombre: " + pokemon[game[gamesCounter].getPokemonsPlayer2()[contador][0]].getName());
            System.out.println("Vida: " + lifePlayer2[contador]);
            System.out.println("Ataque: " + pokemon[game[gamesCounter].getPokemonsPlayer2()[contador][0]].getAttakPoints());
            if (lifePlayer2[contador] > 0) {
                System.out.println("Estado: Vivo");
            } else {
                System.out.println("Estado: Muerto");
            }
            System.out.println("----------------------");
        }

    }
    
    public void save(){
        Principal.game = game;
        Principal.gamesCounter = gamesCounter;
    }

}
