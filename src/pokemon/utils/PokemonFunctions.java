/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pokemon.utils;

import java.util.Random;
import java.util.Scanner;
import pokemon.beans.Pokemon;

/**
 *
 * @author Alejandro Barrios
 */
public class PokemonFunctions {

    public Pokemon pokemon[] = new Pokemon[6];
    Scanner selection = new Scanner(System.in);

    public void startPokemons() {

        String[] pokemonNames = {"Pikachu",
            "Squirtle",
            "Charmander",
            "Bulbasaur",
            "Chikorita",
            "Eevee"};

        for (int contador = 0; contador < pokemonNames.length; contador++) {
            // Get random life points
            Random lifeRandom = new Random();
            int lifePointsRandom = lifeRandom.nextInt(100 - 50) + 50;

            // Get random atk points
            Random atkRandom = new Random();
            int atkPointsRandom = atkRandom.nextInt(20 - 5) + 5;

            pokemon[contador] = new Pokemon();
            pokemon[contador].setId(contador);
            pokemon[contador].setName(pokemonNames[contador]);
            pokemon[contador].setLifePoints(lifePointsRandom);
            pokemon[contador].setAttakPoints(atkPointsRandom);
            pokemon[contador].setImgPokemon(Utility.getImageAscii(contador));
            pokemon[contador].setTimesPlayed(0);

        }
    }

    public void pokedex() {
        for (int counter = 0; counter < pokemon.length; counter++) {
            System.out.println("--------------------------------------------------");
            System.out.println("Nombre: " + pokemon[counter].getName());
            System.out.println("Puntos de vida: " + pokemon[counter].getLifePoints());
            System.out.println("Puntos de ataque: " + pokemon[counter].getAttakPoints());
            System.out.println("");
            System.out.println(pokemon[counter].getImgPokemon());
            System.out.println("--------------------------------------------------");
            System.out.println("");
        }
    }

    public void editPokemon() {
        System.out.println("--------------------------------------------------");
        System.out.println("Selecciona el pokemon que deseas editar");
        for (int counter = 0; counter < pokemon.length; counter++) {
            System.out.println((counter + 1) + "." + pokemon[counter].getName());
        }
        System.out.print("> ");

        int selected = 0;
        try {
            selection.reset();
            selected = selection.nextInt();
            if (selected > pokemon.length || selected < 1) {
                System.out.println("Ingresa un número entre 1 y " + pokemon.length);
                editPokemon();
            }

        } catch (Exception e) {
            System.out.println("Ingresa un número entre 1 y " + pokemon.length);
            selection.next();
            editPokemon();
        }

        selected -= 1;
        editSelection(selected);

    }

    private void editSelection(int selectedPokemon) {
        System.out.println("--------------------------------------------------");
        System.out.println("Selecciona el atributo que deseas editar");
        System.out.println("1. Nombre: " + pokemon[selectedPokemon].getName());
        System.out.println("2. Puntos de vida: " + pokemon[selectedPokemon].getLifePoints());
        System.out.println("3. Puntos de ataque: " + pokemon[selectedPokemon].getAttakPoints());
        System.out.print("> ");

        int secondChoice = 0;
        try {
            secondChoice = selection.nextInt();
            if (secondChoice > 3 || secondChoice < 1) {
                System.out.println("Ingresa un número entre 1 y 3");
                editSelection(selectedPokemon);
            }
        } catch (Exception e) {
            System.out.println("Ingresa un número entre 1 y 3");
            editSelection(selectedPokemon);
        }

        switch (secondChoice) {
            case 1:
                System.out.println("Ingresa un nuevo nombre");
                System.out.print("> ");
                pokemon[selectedPokemon].setName(selection.next());
                break;

            case 2:
                boolean acceptedValue = true;
                int lp = 0;
                do {
                    System.out.println("Ingresa nuevos puntos de vida entre 50 y 100");
                    System.out.print("> ");

                    try {
                        lp = selection.nextInt();

                        if (lp > 100 || lp < 50) {
                            acceptedValue = false;
                        } else {
                            acceptedValue = true;
                        }

                    } catch (Exception e) {
                        acceptedValue = false;
                        selection.next();
                    }
                } while (!acceptedValue);

                pokemon[selectedPokemon].setLifePoints(lp);

                break;

            case 3:
                boolean acceptedAtk = true;
                int atk = 0;
                do {
                    System.out.println("Ingresa los nuevos puntos de ataque entre 5 y 20");
                    System.out.print("> ");

                    try {
                        atk = selection.nextInt();
                        if (atk > 20 || atk < 5) {
                            acceptedAtk = false;
                        } else {
                            acceptedAtk = true;
                        }

                    } catch (Exception e) {
                        acceptedAtk = false;
                        selection.next();
                    }
                } while (!acceptedAtk);

                pokemon[selectedPokemon].setAttakPoints(atk);

                break;
        }

        System.out.println("¿Desea editar otro atributo? si/no");
        System.out.println("(Si escribe algo diferente a 'si', será considerado 'no')");
        System.out.print("> ");
        if (selection.next().equals("si")) {
            editSelection(selectedPokemon);
        }

    }

}
