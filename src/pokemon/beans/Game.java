/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pokemon.beans;

/**
 *
 * @author Alejandro Barrios
 */
public class Game {
    
    private int id;
    private String namePlayer1;
    private String namePlayer2;
    private String winner;
    private int[][] pokemonsPlayer1;
    private int[][] pokemonsPlayer2;

    public Game() {
        
    }

    public Game(int id, String namePlayer1, String namePlayer2, String winner, int[][] pokemonsPlayer1, int[][] pokemonsPlayer2) {
        this.id = id;
        this.namePlayer1 = namePlayer1;
        this.namePlayer2 = namePlayer2;
        this.winner = winner;
        this.pokemonsPlayer1 = pokemonsPlayer1;
        this.pokemonsPlayer2 = pokemonsPlayer2;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getNamePlayer1() {
        return namePlayer1;
    }

    public void setNamePlayer1(String namePlayer1) {
        this.namePlayer1 = namePlayer1;
    }

    public String getNamePlayer2() {
        return namePlayer2;
    }

    public void setNamePlayer2(String namePlayer2) {
        this.namePlayer2 = namePlayer2;
    }

    public String getWinner() {
        return winner;
    }

    public void setWinner(String winner) {
        this.winner = winner;
    }

    public int[][] getPokemonsPlayer1() {
        return pokemonsPlayer1;
    }

    public void setPokemonsPlayer1(int[][] pokemonsPlayer1) {
        this.pokemonsPlayer1 = pokemonsPlayer1;
    }

    public int[][] getPokemonsPlayer2() {
        return pokemonsPlayer2;
    }

    public void setPokemonsPlayer2(int[][] pokemonsPlayer2) {
        this.pokemonsPlayer2 = pokemonsPlayer2;
    }
    
}
