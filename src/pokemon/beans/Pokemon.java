/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pokemon.beans;

/**
 *
 * @author Alejandro Barrios
 */
public class Pokemon {
    
    // Atributos del pokemon
    private int id;
    private String name;
    private int lifePoints;
    private int attakPoints;
    private String imgPokemon;
    private int timesPlayed;
    
    // Constructor vacio
    public Pokemon(){
        
    }

    // Constructor con parametros
    public Pokemon(int id, String name, int lifePoints, int attakPoints, String imgPokemon, int timesPlayed) {
        this.id = id;
        this.name = name;
        this.lifePoints = lifePoints;
        this.attakPoints = attakPoints;
        this.imgPokemon = imgPokemon;
        this.timesPlayed = timesPlayed;
    }

    // Getters y Setters

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }
    
    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getLifePoints() {
        return lifePoints;
    }

    public void setLifePoints(int lifePoints) {
        this.lifePoints = lifePoints;
    }

    public int getAttakPoints() {
        return attakPoints;
    }

    public void setAttakPoints(int attakPoints) {
        this.attakPoints = attakPoints;
    }

    public String getImgPokemon() {
        return imgPokemon;
    }

    public void setImgPokemon(String imgPokemon) {
        this.imgPokemon = imgPokemon;
    }

    public int getTimesPlayed() {
        return timesPlayed;
    }

    public void setTimesPlayed(int timesPlayed) {
        this.timesPlayed = timesPlayed;
    }
    
    
    
}
